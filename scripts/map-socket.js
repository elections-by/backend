let socket = null;

function connectSocket() {
    if (socket === null) {
        socket = io();
    }
}

function subscribeUserPoints() {
    socket.on('onUserPointsUpdated', function (socketMessage) {
        handleUserSocketMessage(socketMessage)
    });
}

function subscribePickets() {
    socket.on('onPicketsUpdated', function (socketMessage) {
        handlePicketSocketMessage(socketMessage)
    });
}


function handleUserSocketMessage(socketMessage) {
    socketMessage.forEach((message) => {
        const type = message.type;
        switch (type) {
            case 'Add':
            case 'Update': {
                const point = message.point;
                const id = point.id;

                const el = document.createElement('div');
                el.className = 'marker';
                el.id = 'marker-' + id;

                const latLng = [point.location.longitude, point.location.latitude];
                const properties = {
                    type: 'userLocation',
                    id: id,
                    name: point.name,
                    lastUpdatedTime: point.lastUpdatedTime
                };

                addOrUpdateMarker(el, id, latLng, properties, true);
            }
                break;
            case 'Delete': {
                deleteMarker(message.pointId);
            }
        }
    })
}

function handlePicketSocketMessage(socketMessage) {
    socketMessage.forEach((message) => {
        const type = message.type;
        switch (type) {
            case 'Add':
            case 'Update': {
                const picket = message.point;
                const id = picket.id;
                const properties = {
                    type: 'picket',
                    address: picket.address,
                    lastUpdatedTime: picket.lastUpdatedTime,
                    startTime: picket.startTime,
                    endTime: picket.endTime,
                    description: picket.description
                };
                const el = document.createElement('div');
                el.className = 'marker-cluster-active';
                el.id = 'picket-' + id;

                const latLng = [picket.location.longitude, picket.location.latitude];
                addOrUpdateMarker(el, id, latLng, properties, isOpened(picket.startTime, picket.endTime));
            }
                break;
            case 'Delete': {
                deleteMarker(message.pointId);
            }
        }
    })
}

function toHours(timeOfDay) {
    const time = new Date(timeOfDay._seconds * 1000);
    return time.getHours() + ":00"
}

function isOpened(startTimeObj, endTimeObj) {
    const startTime = new Date(startTimeObj._seconds * 1000);
    const now = new Date();
    const endTime = new Date(endTimeObj._seconds * 1000);
    return startTime <= now && now <= endTime;
}

function toDateTime(lastUpdatedTime) {
    const time = new Date(lastUpdatedTime._seconds * 1000);
    return jQuery.timeago(time);
}

function stringToDate(time) {
    const hours = parseInt(time.split(':')[0]);
    if (!hours) {
        throw 'Invalid time format.'
    }
    const now = new Date();

    now.setSeconds(0);
    now.setMinutes(0);
    now.setMilliseconds(0);
    now.setHours(hours);
    return now;
}

