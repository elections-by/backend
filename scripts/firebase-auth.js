function initFirebase(firebaseConfig) {
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
}

function googleSignIn() {
    const auth = firebase.auth();
    auth.setPersistence(firebase.auth.Auth.Persistence.NONE);
    auth.useDeviceLanguage();
    return auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
        .then(function (result) {
            const user = result.user;
            return user.getIdToken(true)
        })
        .then(function (token) {
            return authorize(token)
        })
}

function authorize(idToken) {
    const data = JSON.stringify({
        idToken: idToken,
    });
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/auth',
            headers: {
                'CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content') || null,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: data,
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                reject(error)
            }
        });
    })
}


function initFirebaseAuth() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            console.log("User signed in", user);
        } else {
            console.log("User isn't signed", user);
        }
    })
}
