class PicketState {
    constructor() {
        this.picket = null;
        this.picketMarker = null;

        this.isAllowedUserToCreate = false;
        this.zoom = -1;
        this.callback = null;
        this.picketFormCallback = null;
        this.zoomTh = 16;
    }

    isAddPicketButtonVisible() {
        if (this.zoom < this.zoomTh) {
            return false;
        } else if (this.isPicketCreated()) {
            return false;
        } else if (this.isPicketMarkerCreated()) {
            return false;
        }

        return this.isAllowedUserToCreate;
    }

    isPicketCreated() {
        return !(!this.picket);
    }

    isPicketMarkerCreated() {
        return !(!this.picketMarker);
    }

    isPicketButtonEnabled() {
        if (!this.isAllowedUserToCreate) {
            return false;
        } else if (this.isPicketMarkerCreated()) {
            return true;
        } else if (this.isPicketCreated()) {
            return true;
        }
        return false;
    }

    isControlButtonVisible() {
        return this.isPicketCreated();
    }

    setCallback(callback) {
        this.callback = callback;
    }

    setPicketFromUpdatedCallback(callback) {
        this.picketFormCallback = callback;
    }

    notifyChanges() {
        if (this.callback) {
            this.callback(this);
        }
    }

    setZoom(zoom) {
        if (this.zoom === zoom) {
            return
        }
        const zoomOut = ((this.zoom <= this.zoomTh) && (zoom >= this.zoomTh));
        const zoomIn = ((this.zoom >= this.zoomTh) && (zoom <= this.zoomTh));

        this.zoom = zoom;
        if (zoomOut || zoomIn) {
            this.notifyChanges();
        }
    }

    onPicketCreated(picket) {
        if (this.picketMarker) {
            this.picketMarker.remove();
            this.picketMarker = null;
        }
        this.picket = picket;
        this.notifyChanges();

        if (this.picketFormCallback) {
            this.picketFormCallback(picket);
        }
    }

    onPicketDestroyed() {
        this.picket = null;
        this.notifyChanges();
    }

    setUserPermission(isUserHasPermission) {
        if (this.isAllowedUserToCreate === !!isUserHasPermission) {
            return;
        }
        this.isAllowedUserToCreate = !!isUserHasPermission;
        this.notifyChanges();
    }

    onPicketMarkerAdded(picketMarker) {
        if (this.picketMarker) {
            this.picketMarker.remove();
            this.picketMarker = picketMarker;
            return
        }
        this.picketMarker = picketMarker;
        this.notifyChanges();
    }

    onPicketMarkerRemoved() {
        if (!this.picketMarker) {
            return
        }
        this.picketMarker = null;
        this.notifyChanges();
    }
}

const picketState = new PicketState();

function addPicketMarker() {
    const coords = map.getCenter();
    const style = document.createElement('div');
    style.className = 'marker-cluster-inactive';
    const marker = new mapboxgl.Marker(style)
        .setLngLat(coords)
        .setDraggable(true)
        .addTo(map);

    picketState.onPicketMarkerAdded(marker);
}

function removePicketMarker() {
    if (picketState.isPicketMarkerCreated()) {
        picketState.picketMarker.remove();
        picketState.onPicketMarkerRemoved();
    }
}

function subscribeMyPickets() {
    socket.on('onPicketUpdated', function (message) {
        console.log('My pickets: update', message);
        const type = message.type;
        switch (type) {
            case 'Add':
            case 'Update': {
                const picket = message.point;
                const id = picket.id;
                picketState.onPicketCreated(picket);
            }
                break;
            case 'Delete': {
                const id = message.pointId;
                picketState.onPicketDestroyed();
            }
                break;
        }
    });
}

function subscribeUser() {
    return new Promise((resolve, reject) => {
        socket.emit('login', {payload: 'Hello'},
            function (responseData) {
                resolve(responseData)
            });
    });
}


function sendPicket(picket) {
    const data = JSON.stringify(picket);
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/create/newPicket',
            headers: {
                'CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content') || null,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: data,
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                reject(error)
            }
        });
    })
}

function deletePicket(id) {
    const data = JSON.stringify({
        id: id,
        time: new Date()
    });
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/create/deletePicket',
            headers: {
                'CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content') || null,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: data,
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                reject(error)
            }
        });
    })
}

