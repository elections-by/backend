// mapboxgl.Map
let map;
const markers = new Map();

function createMap(onLoad) {
    mapboxgl.accessToken = document.querySelector('meta[name="mapbox-access-token"]').getAttribute('content') || null;
    map = new mapboxgl.Map({
        container: 'map',
        zoom: 10,
        center: [27.5666644, 53.8999964],
        style: 'mapbox://styles/mapbox/streets-v11',
        maxBounds: [21.5, 51, 35, 57]
    });

    map.addControl(
        new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true
        })
    );


    map.on('load', function () {
        console.log('A load event occurred.');
        if (onLoad) {
            onLoad()
        }
    });
}

function createPopup(html, lngLat) {
    const popup = new mapboxgl.Popup({offset: 25, closeOnClick: false})
        .setLngLat(lngLat)
        .setHTML(html)
        .addTo(map);
}

function sendForm(form) {
    const data = JSON.stringify(form);
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/form/apply',
            headers: {
                'CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content') || null,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: data,
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                reject(error)
            }
        });
    })
}

function getPopup(properties) {
    let html = '';
    if (properties.type === 'picket') {
        html = `
<div class="container">
        <br/>
        <h6 class="card-subtitle mb-2 text-muted label">Пункт сбора подписей</h6>
        <h5 class="card-title description">${properties.address}</h5>
        <hr/>
        <h6 class="card-subtitle mb-2 text-muted label">Время работы</h6>
        <h5 class="card-title description">${toHours(properties.startTime)}-${toHours(properties.endTime)}</h5>
        <hr/>
        <h6 class="text-muted label" class="card-text">Описание</>
        <h5 class="card-title description ">${properties.description}</h5>
</div>
`;
    } else {
        html = `
<div class="container" ">
        <h5 class="card-title">${properties.name}</h5>
        <p class="card-text">Последнее обновление ${toDateTime(properties.lastUpdatedTime)}.</p>
        <a href="#" class="btn btn-outline-primary" onclick="doRequest('${properties.id}')">Оставить заявку</a>
 </div>
`;
    }
    return new mapboxgl.Popup({offset: 25}).setHTML(html);
}

function addOrUpdateMarker(style, id, lngLat, properties, isOpened) {
    let marker = markers.get(id);
    if (marker === undefined) {
        marker = new mapboxgl.Marker(style)
            .setLngLat(lngLat)
            .setPopup(getPopup(properties))
            .addTo(map);

        marker.properties = properties;
        markers.set(id, marker)
    } else {
        marker.properties = properties;
        marker.setLngLat(lngLat);
        marker.setPopup(getPopup(properties))
    }
    updateMarker(marker, isOpened);
    return marker
}

function updateMarker(marker, isOpened) {
    const classList = marker.getElement().classList;
    if (isOpened) {
        classList.add('marker-cluster-active');
        classList.remove('marker-cluster-inactive');
    } else {
        classList.add('marker-cluster-inactive');
        classList.remove('marker-cluster-active');
    }
}

function deleteMarker(id) {
    const marker = markers.get(id);
    if (marker === undefined) {
        console.log('Can not delete marker', id);
        return false
    } else {
        console.log('Delete marker:', id);
        marker.remove();
        markers.delete(id);
        return true
    }
}