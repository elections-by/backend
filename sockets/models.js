const Location = function (geoPoint) {
    this.latitude = geoPoint._latitude;
    this.longitude = geoPoint._longitude;
};
module.exports.Location = Location;

const Point = function (documentData) {
    this.id = documentData.id;
    this.name = documentData.userName;
    this.lastUpdatedTime = documentData.lastUpdatedTime;
    this.location = new Location(documentData.location);
};
module.exports.Point = Point;

const Picket = function (documentData) {
    this.id = documentData.id;
    this.address = documentData.address;
    this.startTime = documentData.startTime;
    this.endTime = documentData.endTime;
    this.description = documentData.description;
    this.lastUpdatedTime = documentData.lastUpdatedTime;
    this.location = new Location(documentData.location);
};
module.exports.Picket = Picket;

const AddPointMessage = function (point) {
    this.type = 'Add';
    this.point = point;
};
module.exports.AddPointMessage = AddPointMessage;

const UpdatePointMessage = function (point) {
    this.type = 'Update';
    this.point = point;
};
module.exports.UpdatePointMessage = UpdatePointMessage;

const DeleteMessage = function (pointId) {
    this.type = 'Delete';
    this.pointId = pointId;
};
module.exports.DeleteMessage = DeleteMessage;

