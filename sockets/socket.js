const modules = require('../modules'), database = modules.database, admin = modules.admin,
    io = modules.io, csrfProtection = modules.csrfProtection, models = require('./models'),
    AddPointMessage = models.AddPointMessage, DeleteMessage = models.DeleteMessage,
    UpdatePointMessage = models.UpdatePointMessage, Picket = models.Picket, Point = models.Point,
    allPoints = new Map(),
    allPickets = new Map(),
    admins = new Map();

database.collection("userPoints").onSnapshot(querySnapshot => {
    try {
        const messages = querySnapshot.docChanges().map(snapShot => {
            const value = new Point(snapShot.doc.data());
            console.log(value);
            switch (snapShot.type) {
                case "added": {
                    const message = new AddPointMessage(value);
                    allPoints.set(value.id, message);
                    return message;
                }
                case "removed": {
                    allPoints.delete(value.id);
                    return new DeleteMessage(value.id);
                }
                case "modified": {
                    const message = new UpdatePointMessage(value);
                    allPoints.set(value.id, message);
                    return message;
                }
            }
        });
        io.sockets.emit('onUserPointsUpdated', messages);
    } catch (e) {
        console.log("On error: ", e);
    }
});

database.collection("pickets").onSnapshot(querySnapshot => {
    try {
        const messages = querySnapshot.docChanges().map(snapShot => {
            const value = new Picket(snapShot.doc.data());
            console.log('Picket', value, value.id);
            switch (snapShot.type) {
                case "added": {
                    const message = new AddPointMessage(value);
                    allPickets.set(value.id, message);
                    notifyAdmin(value.id, message);
                    return message;
                }
                case "removed": {
                    const message = new DeleteMessage(value.id);
                    allPickets.delete(value.id);
                    notifyAdmin(value.id, message);
                    return message;
                }
                case "modified": {
                    const message = new UpdatePointMessage(value);
                    allPickets.set(value.id, message);
                    notifyAdmin(value.id, message);
                    return message;
                }
            }
        });
        io.sockets.emit('onPicketsUpdated', messages);
    } catch (e) {
        console.log("On error: ", e);
    }
});

function notifyAdmin(id, message) {
    const socket = admins.get(id);
    if (socket) {
        console.log('Send private message to ', id);
        socket.emit('onPicketUpdated', message);
    } else {
        console.log('Error: user not found ', id);
    }
}

io.on('connection', (socket) => {
    socket.emit('onUserPointsUpdated', Array.from(allPoints.values()));
    socket.emit('onPicketsUpdated', Array.from(allPickets.values()));
    socket.on('disconnect', function () {
        console.log('User is disconnected');
        const user = socket.handshake.session.user;
        if (user && user.sessionCookie) {
            admins.delete(user.uuid)
        }
    });

    socket.on("login", function (data, callback) {
        const user = socket.handshake.session.user;
        if (user && user.sessionCookie) {
            console.log(user);
            getRole(user.sessionCookie)
                .then(role => {
                    callback({
                        id: user.uuid,
                        role: role,
                        picket: allPickets.get(user.uuid),
                        userPoints: allPoints.get(user.uuid)
                    });

                    if (role === 'Admin' || role === 'Member') {
                        console.log('Add socket for user ', user.uuid);
                        admins.set(user.uuid, socket);
                    }
                })
                .catch(error => {
                    console.log('Error:', error);
                    callback('Guest')
                })
        }
    });
});


function getRole(sessionCookie) {
    return admin.auth().verifySessionCookie(sessionCookie, true)
        .then((decodedClaims) => {
            return database.collection('roles').doc(decodedClaims.user_id).get()
        }).then((role) => {
            if (role.exists) {
                return role.data().type || 'Guest';
            } else {
                return 'Guest';
            }
        });
}

