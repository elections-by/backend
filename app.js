'use strict';
const modules = require('./modules');
const server = modules.server;
const routes = require('./routes');
const sockets = require('./sockets/socket');


modules.app.use("/", routes);

if (module === require.main) {
    const PORT = process.env.PORT || 8080;
    server.listen(PORT, () => {
        console.log(`App listening on port ${PORT}`);
        console.log('Press Ctrl+C to quit.');
    });
}