# PoC of decentralized management for point of interest
  

This sample demonstrates how to create own portal with easily management POIs. 
[Google App Engine Flexible Environment][appengine] with Node.js.

* [Setup](#setup)
* [Running locally](#running-locally)
* [Deploying to App Engine](#deploying-to-app-engine)

## Setup

Before you can run or deploy the sample, you need to do the following:

- [ ] Create project on Google Cloud (https://console.cloud.google.com/)
- [ ] Create firestore database on Firebase Console (https://console.firebase.google.com/)
- [ ] Deploy Firebase functions (https://gitlab.com/elections-by/firebase-functions)
- [ ]  Enable Google Sign-In in the Firebase console:
> In the Firebase console, open the Auth section.
>
> On the Sign in method tab, enable the Google sign-in method and click Save.
- [ ] Create and download service account (https://console.firebase.google.com/project/*{project_name}*/settings/serviceaccounts/adminsdk) and save to secrets/firebase-key.json
- [ ] Add web app in Firebase settings and copy config for web app (https://firebase.google.com/docs/web/setup)
- [ ] Create mapbox account (https://account.mapbox.com/auth/signup/)
- [ ] Create mapbox access token (https://account.mapbox.com/access-tokens/create)
- [ ] Create keys.json file in **secrets/** folder
- [ ] Fill keys.json file 

```javascript
const serviceAccount = require('./firebase-key.json'); 

module.exports = {
    session: {
        key: 'user_sid',
        secret: '<ANY_SECRET_TO_ENCRYPT>',
    },

    mapbox: {
        accessToken: '<MAPBOX_ACCESS_TOKEN>'
    },

    firebaseConfig: '{ .... # Web app in Firebase settings }',

    firebaseAdmin: {
        certificate: serviceAccount,
        databaseURL: '<database url from Firebase Admin SDK >' 
    }
}; 
```
## Install dependencies:
    npm install

## Running locally
    npm start

## Deploying to App Engine
    npm run deploy

