const express = require('express'), csrf = require('csurf'), cookieParser = require('cookie-parser'), app = express(),
    keys = require("./secrets/keys"), admin = require("firebase-admin"),
    expiresIn = 60 * 60 * 24 * 5 * 1000, csrfProtection = csrf({cookie: true}), server = require('http').Server(app),
    io = require('socket.io')(server),
    sharedsession = require("express-socket.io-session");


const session = require('express-session')({
    key: keys.session.key,
    secret: keys.session.secret,
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: expiresIn
    }
});

app.set('view engine', 'pug');
app.use(express.json());
app.use(express.urlencoded());
app.use(cookieParser());
app.use(csrf({cookie: true}));
app.use(express.static(__dirname + '/scripts'));
app.use(express.static(__dirname + '/assets'));
app.use(express.static(__dirname + '/icons'));


// Attach session
app.use(session);

// Share session with io sockets
io.use(sharedsession(session));

// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after auth, your cookie still remains saved in the browser.
app.use((req, res, next) => {
    if (req.cookies.session && !req.session.user) {
        res.clearCookie('session');
    }
    next();
});

// middleware function to check for logged-in users
const sessionChecker = (req, res, next) => {
    if (req.session.user && req.cookies.session) {
        res.redirect('/auth');
    } else {
        next();
    }
};

admin.initializeApp({
    credential: admin.credential.cert(keys.firebaseAdmin.certificate),
    databaseURL: keys.firebaseAdmin.databaseURL
});


module.exports.app = app;
module.exports.csrfProtection = csrfProtection;
module.exports.sessionChecker = sessionChecker;
module.exports.admin = admin;
module.exports.database = admin.firestore();
module.exports.expiresIn = expiresIn;
module.exports.io = io;
module.exports.server = server;
module.exports.keys = keys;



