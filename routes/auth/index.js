const auth = require('express').Router(), modules = require('../../modules'), admin = modules.admin,
    csrfProtection = modules.csrfProtection, database = modules.database, key = modules.keys,
    expiresIn = modules.expiresIn;


auth.get('/', csrfProtection, (req, res) => {
    res.render('auth.pug', {
        csrfToken: req.csrfToken(),
        firebaseConfig: JSON.stringify(key.firebaseConfig)
    });
});

auth.post('/', (req, res) => {
    const body = req.body;
    const idToken = body.idToken.toString();
    const user = {
        lastSignIn: new Date()
    };
    admin.auth().verifyIdToken(idToken)
        .then((decodedToken) => {
            user.uuid = decodedToken.uid;
            user.name = decodedToken.name;
            user.photoUrl = decodedToken.picture;
            user.email = decodedToken.email;
            return database.collection('users').doc(user.uuid).set(user)
        })
        .then((result) => {
            return admin.auth().createSessionCookie(idToken, {expiresIn})
        })
        .then((sessionCookie) => {
            user.sessionCookie = sessionCookie;
            req.session.user = user;
            console.log('Success auth:' + user.uuid);
            res.end(JSON.stringify({status: 'success'}));
        })
        .catch((error) => {
            console.log('Auth error:' + user.uuid, error);
            res.status(401).send('UNAUTHORIZED REQUEST!');
        });
});

module.exports = auth;
