const map = require('express').Router(), modules = require('../../modules'), csrfProtection = modules.csrfProtection,
    keys = modules.keys;

map.get('/', csrfProtection, (req, res) => {
    const meta = {
        csrfToken: req.csrfToken(),
        accessToken: keys.mapbox.accessToken
    };
    res.render('map.pug', meta);
});

module.exports = map;
