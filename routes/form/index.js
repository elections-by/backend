const form = require('express').Router(), modules = require('../../modules'), database = modules.database,
    csrfProtection = modules.csrfProtection;


form.post('/apply', csrfProtection, (req, res) => {
    const body = req.body;
    console.log(JSON.stringify(body));
    if (!isValidForm(body)) {
        res.status(400).send('Form is invalid');
        return
    }
    const userId = body.userId;
    const userRef = database.collection('users').doc(userId);
    userRef.get()
        .then(doc => {
            if (doc.exists) {
                console.log("Document data:", doc.data());
                delete body['userId'];
                body.created = new Date();
                body.userReference = userRef;
                body.applied = false;
                return database.collection('requests').add(body)
            } else {
                throw "User is not exist";
            }
        })
        .then(doc => {
            userRef.collection('request').doc(doc.id).set({
                requestReference: database.collection('request').doc(doc.id)
            })
        })
        .then(result => {
            res.end(JSON.stringify({status: 'success'}));
        })
        .catch(error => {
            res.status(401).send(error.toString());
        });
});

function isValidForm(data) {
    try {
        if (data['firstName'].length < 2) {
            return false;
        }
        if (data['lastName'].length < 2) {
            return false;
        }
        if (data['patronymic'].length < 2) {
            return false;
        }
        if (data['region'].length < 2) {
            return false;
        }
        if (data['address'].length < 2) {
            return false;
        }
        if (data['phone'].length < 2) {
            return false;
        }
        return data['userId'].length !== 0;

    } catch (e) {
        return false
    }
}

module.exports = form;
