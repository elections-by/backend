const routes = require('express').Router();


const create = require('./create');
const auth = require('./auth');
const map = require('./map');
const form = require('./form');

routes.use('/create', create);
routes.use('/auth', auth);
routes.use('/', map);
routes.use('/form', form);

module.exports = routes;