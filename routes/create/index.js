const create = require('express').Router(), modules = require('../../modules'), admin = modules.admin,
    database = modules.database, csrfProtection = modules.csrfProtection, sessionChecker = modules.sessionChecker,
    keys = modules.keys;

create.get('/', csrfProtection, sessionChecker, (req, res) => {
    const user = req.session.user || {};
    const sessionCookie = user.sessionCookie;
    if (sessionCookie) {
        admin.auth().verifySessionCookie(
            sessionCookie, true)
            .then((decodedClaims) => {
                const uuid = decodedClaims.user_id;
                return database.collection('roles').doc(uuid).get()
            })
            .then((roleSnapshot) => {
                const meta = {
                    csrfToken: req.csrfToken(),
                    accessToken: keys.mapbox.accessToken
                };
                if (!roleSnapshot.exists) {
                    console.log('User role was not created', user.uuid);
                    res.render('welcome.pug', meta);
                } else {
                    const role = roleSnapshot.data().type || 'Guest';
                    if (role === 'Guest') {
                        res.render('welcome.pug', meta);
                    } else {
                        res.render('create.pug', meta);
                    }
                }
            })
            .catch(error => {
                // Session cookie is unavailable or invalid. Force user to auth.
                res.redirect('/auth');
            });
    } else {
        res.redirect('/auth');
    }
});

create.post('/newPicket', (req, res) => {
    const body = req.body;
    console.log(body);
    const user = req.session.user || {};
    const sessionCookie = user.sessionCookie;

    const point = {
        location: new admin.firestore.GeoPoint(body.location[1], body.location[0]),
        lastUpdatedTime: new Date(),
        address: body.address,
        description: body.description
    };

    try {
        point.startTime = new Date(body.startTime);
        point.endTime = new Date(body.endTime);
    } catch (error) {
        res.status(401).send(error);
        return;
    }

    if (sessionCookie) {
        admin.auth().verifySessionCookie(
            sessionCookie, true /** checkRevoked */)
            .then((decodedClaims) => {
                point.id = decodedClaims.user_id;
                point.userName = decodedClaims.name;
                return database.collection('roles').doc(point.id).get()
            })
            .then((roleSnapshot) => {
                if (!roleSnapshot.exists) {
                    throw 'User is not exist';
                } else {
                    const role = roleSnapshot.data().type || 'Guest';
                    if (role === 'Guest') {
                        throw 'User is not exist';
                    }
                    return database.collection('points').doc(point.id).set(point);
                }
            })
            .then((pointSnapshot) => {
                res.end(JSON.stringify({status: 'success'}));
            })
            .catch(error => {
                res.status(401).send(error);
            });
    } else {
        res.status(401).send('UNAUTHORIZED REQUEST!');
    }
});

create.post('/deletePicket', (req, res) => {
    const body = req.body;
    console.log(body);
    const user = req.session.user || {};
    const sessionCookie = user.sessionCookie;
    if (sessionCookie) {
        admin.auth().verifySessionCookie(
            sessionCookie, true /** checkRevoked */)
            .then((decodedClaims) => {
                const id = decodedClaims.user_id;
                return database.collection('points').doc(id).delete()
            })
            .then((pointSnapshot) => {
                res.end(JSON.stringify({status: 'success'}));
            })
            .catch(error => {
                res.status(401).send(error);
            });
    } else {
        res.status(401).send('UNAUTHORIZED REQUEST!');
    }
});

module.exports = create;